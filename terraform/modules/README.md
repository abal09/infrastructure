# modules/
Contains 2 levels of subdirectories to organize specific modules by type.  The top level should be the type of module (e.g. clusters, networks), and the next level should contain the specific implementation (e.g. Kafka cluster, spoke network)

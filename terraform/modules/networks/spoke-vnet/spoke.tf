resource "azurerm_resource_group" "spoke_rg" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

resource "azurerm_virtual_network" "spoke_vnet" {
  name                = var.vnet_name
  resource_group_name = azurerm_resource_group.spoke_rg.name
  location            = azurerm_resource_group.spoke_rg.location
  address_space       = [var.vnet_cidr]
  tags                = var.tags
}

resource "azurerm_role_assignment" "network_owner" {
  principal_id         = var.owner_group_id
  scope                = azurerm_resource_group.spoke_rg.id
  role_definition_name = "Owner"
}

resource "azurerm_role_assignment" "network_reader" {
  principal_id         = var.reader_group_id
  scope                = azurerm_resource_group.spoke_rg.id
  role_definition_name = "Reader"
}

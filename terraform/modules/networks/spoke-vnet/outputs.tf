output "vnet_id" {
  description = "Spoke virtual network id"
  value       = azurerm_virtual_network.spoke_vnet.id
}

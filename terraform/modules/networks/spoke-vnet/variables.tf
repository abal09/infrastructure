variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

variable "subscription_id" {
  description = "Azure subscription id to assign resources to"
  type        = string
}

variable "location" {
  description = "Azure location to use"
  type        = string
}

variable "resource_group_name" {
  description = "Name for the spoke resource group"
  type        = string
}

variable "vnet_name" {
  description = "Name for the virtual network"
  type        = string
}

variable "vnet_cidr" {
  description = "Address space (CIDR) for the virtual network"
  type        = string
}

variable "hub_vnet_id" {
  description = "Optional hub vnet to enable peering for (default empty string = no peering)"
  type        = string
  default     = ""
}

variable "owner_group_id" {
  description = "Network resource owner security group id"
  type        = string
}

variable "reader_group_id" {
  description = "Network resource reader security group id"
  type        = string
}

variable "tags" {
  description = "Tags to apply to all resources"
  type        = map(string)
  default     = {}
}

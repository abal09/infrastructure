locals {
  # resources in this file won't be created if "hub_net_id" has not been set
  hub_peering_enabled = var.hub_vnet_id == "" ? 0 : 1

  # extract required hub identifiers from its resource id
  hub_vnet_id_split       = split("/", var.hub_vnet_id)
  hub_subscription_id     = element(local.hub_vnet_id_split, 2)
  hub_resource_group_name = element(local.hub_vnet_id_split, 4)
  hub_vnet_name           = element(local.hub_vnet_id_split, 8)

  # create peering names based on vnet name
  #   if the vnet is "dev-cacn-spoke01-vnet" and the hub is "shared-cacn-hub01-vnet"
  #   then the peering names will be:
  #     "dev-cacn-spoke01-shared-cacn-hub01-peer"
  #     "shared-cacn-hub01-dev-cacn-spoke01-peer" (in the remote vnet's resource group)
  hub_name_split    = split("-", local.hub_vnet_name)
  hub_name_base     = join("-", slice(local.hub_name_split, 0, max(length(local.hub_name_split) - 1, 0)))
  vnet_name_split   = split("-", var.vnet_name)
  vnet_name_base    = join("-", slice(local.vnet_name_split, 0, max(length(local.vnet_name_split) - 1, 0)))
  hub_peering_name  = "${local.hub_name_base}-${local.vnet_name_base}-peer"
  vnet_peering_name = "${local.vnet_name_base}-${local.hub_name_base}-peer"
}

provider "azurerm" {
  alias           = "hub"
  subscription_id = local.hub_subscription_id

  features {}
}

resource "azurerm_virtual_network_peering" "outbound" {
  count = local.hub_peering_enabled

  name                      = local.vnet_peering_name
  resource_group_name       = azurerm_resource_group.spoke_rg.name
  virtual_network_name      = azurerm_virtual_network.spoke_vnet.name
  remote_virtual_network_id = var.hub_vnet_id

  # settings based on: https://docs.microsoft.com/en-us/azure/developer/terraform/hub-spoke-spoke-network
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = false
  use_remote_gateways          = true
}

resource "azurerm_virtual_network_peering" "inbound" {
  count    = local.hub_peering_enabled
  provider = azurerm.hub

  name                      = local.hub_peering_name
  resource_group_name       = local.hub_resource_group_name
  virtual_network_name      = local.hub_vnet_name
  remote_virtual_network_id = azurerm_virtual_network.spoke_vnet.id

  # settings based on: https://docs.microsoft.com/en-us/azure/developer/terraform/hub-spoke-spoke-network
  allow_virtual_network_access = true
  allow_forwarded_traffic      = true
  allow_gateway_transit        = true
  use_remote_gateways          = false
}

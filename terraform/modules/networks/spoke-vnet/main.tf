terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.76.0"
    }
  }

  required_version = ">=1.0.0"
}

provider "azurerm" {
  subscription_id = var.subscription_id

  features {}
}

output "keyvault_reader_group_id" {
  description = "Tenant key vault reader security group id"
  value       = azuread_group.tenant_keyvault_reader.id
}

output "keyvault_manager_group_id" {
  description = "Tenant key vault manager security group id"
  value       = azuread_group.tenant_keyvault_manager.id
}

output "owner_group_id" {
  description = "Tenant resource owner security group id"
  value       = azuread_group.tenant_owner.id
}

output "storage_reader_group_id" {
  description = "Tenant storage reader security group id"
  value       = azuread_group.tenant_storage_reader.id
}

output "storage_manager_group_id" {
  description = "Tenant storage manager security group id"
  value       = azuread_group.tenant_storage_manager.id
}

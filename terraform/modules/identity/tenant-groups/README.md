# Identity Access Management – Tenant AD Groups

Creates AD groups used for reader & management access across all tenants in one environment.

Required inputs:
* `environment` (e.g. `dev` or `prod`)

Outputs:
* group id's for each AD group

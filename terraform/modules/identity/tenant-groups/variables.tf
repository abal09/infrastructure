variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

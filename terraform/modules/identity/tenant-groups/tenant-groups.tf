resource "azuread_group" "tenant_storage_reader" {
  description  = "Group members can access & read ${var.environment} tenant data storage"
  display_name = "${var.environment}-tenant-storage-reader-group"
}

resource "azuread_group" "tenant_storage_manager" {
  description  = "Group members can manage ${var.environment} tenant data storage but cannot change access"
  display_name = "${var.environment}-tenant-storage-manager-group"
}

resource "azuread_group" "tenant_keyvault_reader" {
  description  = "Group members can access & read ${var.environment} tenant key vaults"
  display_name = "${var.environment}-tenant-keyvault-reader-group"
}

resource "azuread_group" "tenant_keyvault_manager" {
  description  = "Group members can manage ${var.environment} tenant key vaults but cannot change access"
  display_name = "${var.environment}-tenant-keyvault-manager-group"
}

resource "azuread_group" "tenant_owner" {
  description  = "Group members have full ownership for all ${var.environment} tenant resources"
  display_name = "${var.environment}-tenant-owner-group"
}

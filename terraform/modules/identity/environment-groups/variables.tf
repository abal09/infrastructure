variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

variable "include_cluster_groups" {
  description = "Set to 'true' to create cluster groups"
  type        = bool
  default     = true
}

variable "include_network_keyvault_groups" {
  description = "Set to 'true' to create network keyvault groups"
  type        = bool
  default     = false
}

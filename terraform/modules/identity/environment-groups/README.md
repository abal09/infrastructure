# Identity Access Management – Environment AD Groups

Creates AD groups used for reader & management access for a given environment's resources.

Required inputs:
* `environment` (e.g. `dev` or `prod`)

Outputs:
* group id's for each AD group

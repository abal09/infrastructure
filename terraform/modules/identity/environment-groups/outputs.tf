output "cluster_reader_group_id" {
  description = "AKS Cluster reader security group id"
  value       = length(azuread_group.cluster_reader) > 0 ? azuread_group.cluster_reader[0].id : null
}

output "cluster_manager_group_id" {
  description = "AKS Cluster manager security group id"
  value       = length(azuread_group.cluster_manager) > 0 ? azuread_group.cluster_manager[0].id : null
}

output "cluster_owner_group_id" {
  description = "AKS Cluster owner security group id"
  value       = length(azuread_group.cluster_owner) > 0 ? azuread_group.cluster_owner[0].id : null
}

output "network_keyvault_reader_group_id" {
  description = "Network key vault reader security group id"
  value       = length(azuread_group.network_keyvault_reader) > 0 ? azuread_group.network_keyvault_reader[0].id : null
}

output "network_keyvault_manager_group_id" {
  description = "Network key vault manager security group id"
  value       = length(azuread_group.network_keyvault_manager) > 0 ? azuread_group.network_keyvault_manager[0].id : null
}

output "network_reader_group_id" {
  description = "Network resource reader security group id"
  value       = azuread_group.network_reader.id
}

output "network_owner_group_id" {
  description = "Network resource owner security group id"
  value       = azuread_group.network_owner.id
}

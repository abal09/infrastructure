locals {
  create_cluster_groups          = var.include_cluster_groups ? 1 : 0
  create_network_keyvault_groups = var.include_network_keyvault_groups ? 1 : 0
}

resource "azuread_group" "cluster_reader" {
  count = local.create_cluster_groups

  description  = "Group members have read-only access to all ${var.environment} clusters"
  display_name = "${var.environment}-cluster-reader-group"
}

resource "azuread_group" "cluster_manager" {
  count = local.create_cluster_groups

  description  = "Group members can manage ${var.environment} clusters but cannot change access"
  display_name = "${var.environment}-cluster-manager-group"
}

resource "azuread_group" "cluster_owner" {
  count = local.create_cluster_groups

  description  = "Group members have full ownership + admin access to all ${var.environment} clusters"
  display_name = "${var.environment}-cluster-owner-group"
}

resource "azuread_group" "network_keyvault_reader" {
  count = local.create_network_keyvault_groups

  description  = "Group members can access & read ${var.environment} network key vaults"
  display_name = "${var.environment}-network-keyvault-reader-group"
}

resource "azuread_group" "network_keyvault_manager" {
  count = local.create_network_keyvault_groups

  description  = "Group members can manage ${var.environment} network key vault data but cannot change access"
  display_name = "${var.environment}-network-keyvault-manager-group"
}

resource "azuread_group" "network_reader" {
  description  = "Group members have read-only access to all ${var.environment} network resources (except keyvaults)"
  display_name = "${var.environment}-network-reader-group"
}

resource "azuread_group" "network_owner" {
  description  = "Group members have full ownership for all ${var.environment} network resources"
  display_name = "${var.environment}-network-owner-group"
}

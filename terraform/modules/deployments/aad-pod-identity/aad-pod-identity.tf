locals {
  # extract AKS cluster identifiers from its resource id
  cluster_id_split            = split("/", var.cluster_id)
  cluster_subscription_id     = element(local.cluster_id_split, 2)
  cluster_resource_group_name = element(local.cluster_id_split, 4)
  cluster_name                = element(local.cluster_id_split, 8)
}

resource "null_resource" "install_aks_preview" {
  # AAD pod identity is still in preview, so we need to enable the feature
  # and update your local CLI to use it
  #
  # Note: its possible for the feature to take some time to register, which
  #       may cause the subsequent step to fail on the first apply
  triggers = {
    cluster_id = var.cluster_id
  }

  provisioner "local-exec" {
    command = <<-EOT
      az feature register --name EnablePodIdentityPreview \
        --namespace Microsoft.ContainerService \
        --subscription ${local.cluster_subscription_id} && \
      az extension add --name aks-preview && \
      az extension update --name aks-preview
    EOT
  }
}

resource "null_resource" "enable_aad_pod_identity" {
  # ideally this will be built into the AKS Terraform resource in the future
  # but for now we have to enable it via the CLI option in preview mode
  depends_on = [
    null_resource.install_aks_preview
  ]

  triggers = {
    cluster_id = var.cluster_id
  }

  provisioner "local-exec" {
    command = <<-EOT
      az aks update --enable-pod-identity \
        --name ${local.cluster_name} \
        --resource-group ${local.cluster_resource_group_name} \
        --subscription ${local.cluster_subscription_id}
    EOT
  }
}

variable "cluster_id" {
  description = "Resource ID of the target AKS cluster"
  type        = string
}

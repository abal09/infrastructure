locals {
  # extract required vnet identifiers from its resource id
  vnet_id_split            = split("/", var.vnet_id)
  vnet_subscription_id     = element(local.vnet_id_split, 2)
  vnet_resource_group_name = element(local.vnet_id_split, 4)
  vnet_name                = element(local.vnet_id_split, 8)
}

provider "azurerm" {
  # for cases where the virtual network is in another subscription
  alias           = "vnet"
  subscription_id = local.vnet_subscription_id

  features {}
}

resource "azurerm_subnet" "subnet" {
  provider             = azurerm.vnet
  name                 = var.subnet_name
  resource_group_name  = local.vnet_resource_group_name
  virtual_network_name = local.vnet_name
  address_prefixes     = [var.subnet_cidr]
}

data "azurerm_resource_group" "vnet_rg" {
  provider = azurerm.vnet
  name     = local.vnet_resource_group_name
}

resource "azurerm_role_assignment" "network_contributor" {
  provider             = azurerm.vnet
  principal_id         = azurerm_kubernetes_cluster.aks.identity[0].principal_id
  scope                = data.azurerm_resource_group.vnet_rg.id
  role_definition_name = "Contributor"
}

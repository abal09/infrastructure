output "cluster_id" {
  description = "AKS cluster id"
  value       = azurerm_kubernetes_cluster.aks.id
}

output "cluster_name" {
  description = "AKS cluster name"
  value       = azurerm_kubernetes_cluster.aks.name
}

output "principal_id" {
  description = "AKS managed identity principal id"
  value       = azurerm_kubernetes_cluster.aks.identity[0].principal_id
}

output "resource_group" {
  description = "AKS resource group name"
  value       = azurerm_resource_group.aks_rg.name
}

output "subnet_id" {
  description = "Cluster subnet id"
  value       = azurerm_subnet.subnet.id
}

locals {
  admin_group_ids = (
    var.manager_group_id != "" ?
    [var.manager_group_id, var.owner_group_id, var.gitaccess_sp_id] :
    [var.owner_group_id]
  )
}

resource "azurerm_resource_group" "aks_rg" {
  name     = var.resource_group_name
  location = var.location
  tags     = var.tags
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = azurerm_resource_group.aks_rg.name
  dns_prefix          = lower(var.cluster_name)
  kubernetes_version  = var.kubernetes_version
  node_resource_group = var.node_resource_group_name

  identity {
    type = "SystemAssigned"
  }

  role_based_access_control {
    enabled = true

    azure_active_directory {
      managed                = true
      admin_group_object_ids = local.admin_group_ids
    }
  }

  default_node_pool {
    name                = "systempool"
    vm_size             = var.system_node_size
    availability_zones  = ["1", "2", "3"]
    enable_auto_scaling = false
    max_pods            = var.system_node_max_pods
    node_count          = var.system_node_count
    os_disk_size_gb     = var.system_node_disk_size
    vnet_subnet_id      = azurerm_subnet.subnet.id
  }

  addon_profile {
    http_application_routing {
      enabled = false
    }
    kube_dashboard {
      enabled = false
    }
  }

  network_profile {
    network_plugin     = "azure"
    network_policy     = "calico"
    dns_service_ip     = var.kubernetes_dns_ip
    docker_bridge_cidr = var.docker_bridge_cidr
    service_cidr       = var.kubernetes_service_cidr
    load_balancer_sku  = "Standard"
    # load_balancer_profile {
    #   managed_outbound_ip_count = 1
    # }
  }

  tags = var.tags
}

# Role assignments for AKS cluster
resource "azurerm_role_assignment" "cluster_reader" {
  count                = var.reader_group_id != "" ? 1 : 0
  principal_id         = var.reader_group_id
  scope                = azurerm_resource_group.aks_rg.id
  role_definition_name = "Reader"
}

resource "azurerm_role_assignment" "cluster_manager" {
  count                = var.manager_group_id != "" ? 1 : 0
  principal_id         = var.manager_group_id
  scope                = azurerm_resource_group.aks_rg.id
  role_definition_name = "Contributor"
}

resource "azurerm_role_assignment" "cluster_owner" {
  principal_id         = var.owner_group_id
  scope                = azurerm_resource_group.aks_rg.id
  role_definition_name = "Owner"
}

data "azurerm_resource_group" "aks_node_rg" {
  name = azurerm_kubernetes_cluster.aks.node_resource_group
}

resource "azurerm_role_assignment" "cluster_node_reader" {
  count                = var.reader_group_id != "" ? 1 : 0
  principal_id         = var.reader_group_id
  scope                = data.azurerm_resource_group.aks_node_rg.id
  role_definition_name = "Reader"
}

resource "azurerm_role_assignment" "cluster_node_manager" {
  count                = var.manager_group_id != "" ? 1 : 0
  principal_id         = var.manager_group_id
  scope                = data.azurerm_resource_group.aks_node_rg.id
  role_definition_name = "Contributor"
}

resource "azurerm_role_assignment" "cluster_node_owner" {
  principal_id         = var.owner_group_id
  scope                = data.azurerm_resource_group.aks_node_rg.id
  role_definition_name = "Owner"
}

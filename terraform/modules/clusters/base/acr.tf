locals {
  default_acr_id = "/subscriptions/db5c09d8-5487-4053-9050-644fabba3fd6/resourceGroups/global-admin-rg/providers/Microsoft.ContainerRegistry/registries/devmainacr"
  acr_id         = (var.acr_id != "") ? var.acr_id : local.default_acr_id

  # extract container registry identifiers from its resource id
  acr_id_split            = split("/", local.acr_id)
  acr_subscription_id     = element(local.acr_id_split, 2)
  acr_resource_group_name = element(local.acr_id_split, 4)
  acr_name                = element(local.acr_id_split, 8)
}

data "azurerm_resource_group" "acr_rg" {
  name     = local.acr_resource_group_name
}

resource "azurerm_role_assignment" "acr_pull" {
  principal_id         = azurerm_kubernetes_cluster.aks.kubelet_identity[0].object_id
  scope                = data.azurerm_resource_group.acr_rg.id
  role_definition_name = "AcrPull"
}

variable "subscription_id" {
  description = "Azure subscription id to assign resources to"
  type        = string
}

variable "location" {
  description = "Azure location to use"
  type        = string
}

variable "resource_group_name" {
  description = "Name for the AKS resource group"
  type        = string
}

variable "node_resource_group_name" {
  description = "Name for the AKS node resource group"
  type        = string
}

variable "cluster_name" {
  description = "Name for the AKS cluster"
  type        = string
}

variable "vnet_id" {
  description = "Resource id of the virtual network to host the cluster within"
  type        = string
}

variable "subnet_name" {
  description = "Name for the cluster subnet"
  type        = string
}

variable "subnet_cidr" {
  description = "Address space (CIDR) for the cluster subnet"
  type        = string
}

variable "kubernetes_version" {
  description = "Kubernetes version to use"
  type        = string
}

variable "acr_id" {
  description = "Azure container registry id to pull images from"
  type        = string
  default     = "" # use default ID if blank
}

variable "docker_bridge_cidr" {
  description = "CIDR for docker bridge IPs (can overlap with other clusters, but not other vnet IPs)"
  type        = string
  default     = "172.17.0.1/16" # AKS default range
}

variable "kubernetes_service_cidr" {
  description = "CIDR for cluster service IPs (can overlap with other clusters, but not other vnet IPs)"
  type        = string
  default     = "172.18.0.0/16"
}

variable "kubernetes_dns_ip" {
  description = "DNS IP address for services; must be inside service CIDR range, but cannot use first IP"
  type        = string
  default     = "172.18.0.10"
}

variable "system_node_size" {
  description = "System pool node size (VM sku)"
  type        = string
  default     = "Standard_DS2_v2"
}

variable "system_node_count" {
  description = "System pool node count"
  type        = number
  default     = 1
}

variable "system_node_disk_size" {
  description = "System pool node disk size (in GB)"
  type        = number
  default     = 512
}

variable "system_node_max_pods" {
  description = "System pool maximum pods per node"
  type        = number
  default     = 100
}

variable "reader_group_id" {
  description = "AKS cluster reader security group id (default is none)"
  type        = string
  default     = ""
}

variable "manager_group_id" {
  description = "AKS cluster manager security group id (default is none)"
  type        = string
  default     = ""
}

variable "owner_group_id" {
  description = "AKS cluster owner security group id (required)"
  type        = string
}

variable "gitaccess_sp_id" {
  description = "Service Principal ID for github"
  type        = string
}

variable "tags" {
  description = "Tags to apply to all resources"
  type        = map(string)
  default     = {}
}

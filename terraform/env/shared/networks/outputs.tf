output "hub_vnet_id" {
  description = "Shared network hub virtual network id"
  value       = azurerm_virtual_network.hub_vnet.id
}

output "vpn_subnet_id" {
  description = "Shared network VPN gateway subnet id"
  value       = azurerm_subnet.vpn_subnet.id
}

locals {
  hub_name = "${var.environment}-${var.primary_location_code}-hub01"

  tags = {
    creator  = "terraform"
    env      = var.environment
    location = var.primary_location_code
    owner    = "devops@devops.io"
  }
}

resource "azurerm_resource_group" "hub_rg" {
  name     = "${local.hub_name}-rg"
  location = var.primary_location
  tags     = local.tags
}

resource "azurerm_virtual_network" "hub_vnet" {
  name                = "${local.hub_name}-vnet"
  resource_group_name = azurerm_resource_group.hub_rg.name
  location            = azurerm_resource_group.hub_rg.location
  address_space       = [var.hub_vnet_cidr]
  tags                = local.tags
}

resource "azurerm_role_assignment" "network_owner" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_owner_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Owner"
}

resource "azurerm_role_assignment" "network_reader" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_reader_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Reader"
}

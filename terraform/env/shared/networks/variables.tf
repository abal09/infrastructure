variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

variable "subscription_id" {
  description = "Azure subscription id to assign resources to"
  type        = string
}

variable "primary_location" {
  description = "Azure primary location name"
  type        = string
}

variable "primary_location_code" {
  description = "devops short code for the primary location"
  type        = string
}

variable "hub_vnet_cidr" {
  description = "Address space (CIDR) for the hub virtual network"
  type        = string
  default     = "10.1.0.0/16"
}

variable "vpn_subnet_cidr" {
  description = "Address space (CIDR) for the VPN gateway subnet"
  type        = string
  default     = "10.1.1.0/27"
}

variable "vpn_client_cidr" {
  description = "Address space (CIDR) for the VPN client pool"
  type        = string
  default     = "172.16.1.0/24"
}

variable "trusted_ips" {
  description = "List of trusted IPs for remote access (e.g. key vault)"
  type        = list(string)
  default     = []
}

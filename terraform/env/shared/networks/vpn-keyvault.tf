locals {
  vault_name = "devops-${substr(var.environment, 0, 1)}-vgateway01"
}

resource "azurerm_role_assignment" "vpn_vault_manager" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_keyvault_manager_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Key Vault Administrator"
}

resource "azurerm_role_assignment" "vpn_vault_reader" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_keyvault_reader_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Key Vault Secrets User"
}

resource "azurerm_key_vault" "vpn_vault" {
  depends_on = [ # "contact" block requires user to have "managecontacts" permission
    azurerm_role_assignment.vpn_vault_manager,
  ]

  name                = "${local.vault_name}-kv"
  location            = azurerm_resource_group.hub_rg.location
  resource_group_name = azurerm_resource_group.hub_rg.name
  sku_name            = "standard"
  tenant_id           = data.azurerm_client_config.current.tenant_id

  enable_rbac_authorization  = true
  purge_protection_enabled   = true
  soft_delete_retention_days = 90

  contact { # requires data plane access to set
    email = "devops@devops.io"
  }

  network_acls {
    # must be secured to only trusted IP's used for generating VPN client certs
    bypass         = "AzureServices"
    default_action = "Deny"
    ip_rules       = var.trusted_ips
  }

  tags = local.tags
}

resource "azurerm_monitor_diagnostic_setting" "keyvault_diag" {
  name               = "${local.vault_name}-diag"
  target_resource_id = azurerm_key_vault.vpn_vault.id
  storage_account_id = azurerm_storage_account.log_storage.id

  log {
    category = "AuditEvent"
    enabled  = true

    retention_policy {
      days    = 365
      enabled = true
    }
  }

  metric {
    category = "AllMetrics"
    enabled  = true

    retention_policy {
      days    = 30
      enabled = true
    }
  }
}

resource "azurerm_key_vault_certificate" "vpn_root_cert" {
  name         = "vpn-root-cert"
  key_vault_id = azurerm_key_vault.vpn_vault.id

  certificate_policy {
    issuer_parameters {
      name = "Self"
    }
    key_properties {
      exportable = true
      key_size   = 4096
      key_type   = "RSA"
      reuse_key  = true
    }
    lifetime_action {
      action {
        action_type = "EmailContacts"
      }
      trigger {
        days_before_expiry = 90
      }
    }
    secret_properties {
      content_type = "application/x-pem-file"
    }
    x509_certificate_properties {
      # Server Authentication = 1.3.6.1.5.5.7.3.1
      # Client Authentication = 1.3.6.1.5.5.7.3.2
      extended_key_usage = ["1.3.6.1.5.5.7.3.1"]

      key_usage = [
        "cRLSign",
        "digitalSignature",
        "keyAgreement",
        "keyCertSign",
        "keyEncipherment",
        "nonRepudiation",
      ]

      subject_alternative_names {
        dns_names = ["vpn.devopsanalytics.com"]
      }

      subject            = "CN=devops Azure VPN,O=devops Analytics,OU=DevOps,C=CA"
      validity_in_months = 60
    }
  }
}

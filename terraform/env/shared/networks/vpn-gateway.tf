locals {
  gateway_name = "${var.environment}-${var.primary_location_code}-vpngateway01"
}

resource "azurerm_subnet" "vpn_subnet" {
  name                 = "GatewaySubnet" # network gatewy requires this to be named "GatewaySubnet"
  resource_group_name  = azurerm_resource_group.hub_rg.name
  virtual_network_name = azurerm_virtual_network.hub_vnet.name
  address_prefixes     = [var.vpn_subnet_cidr]
}

resource "azurerm_public_ip" "vpn_ip" {
  name                = "${local.gateway_name}-pip"
  location            = azurerm_resource_group.hub_rg.location
  resource_group_name = azurerm_resource_group.hub_rg.name
  allocation_method   = "Dynamic"
  tags                = local.tags
}

resource "azurerm_virtual_network_gateway" "vpn" {
  name                = "${local.gateway_name}-vgw"
  location            = azurerm_resource_group.hub_rg.location
  resource_group_name = azurerm_resource_group.hub_rg.name

  active_active = false
  enable_bgp    = false
  sku           = "VpnGw1"
  type          = "Vpn"
  vpn_type      = "RouteBased"

  ip_configuration {
    name                          = "vnetGatewayConfig" # default
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vpn_ip.id
    subnet_id                     = azurerm_subnet.vpn_subnet.id
  }

  vpn_client_configuration {
    address_space        = [var.vpn_client_cidr]
    vpn_client_protocols = ["IkeV2", "OpenVPN"]

    root_certificate {
      name             = "${azurerm_key_vault.vpn_vault.name}.${azurerm_key_vault_certificate.vpn_root_cert.name}"
      public_cert_data = azurerm_key_vault_certificate.vpn_root_cert.certificate_data_base64
    }
  }

  tags = local.tags
}

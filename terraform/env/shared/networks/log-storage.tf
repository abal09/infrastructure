locals {
  storage_name = "devops${substr(var.environment, 0, 1)}logging01"
}

resource "azurerm_storage_account" "log_storage" {
  name                = "${local.storage_name}st"
  location            = azurerm_resource_group.hub_rg.location
  resource_group_name = azurerm_resource_group.hub_rg.name

  account_kind             = "StorageV2"
  account_tier             = "Standard"
  account_replication_type = "LRS"

  access_tier               = "Hot"
  allow_blob_public_access  = false
  enable_https_traffic_only = true
  min_tls_version           = "TLS1_2"

  network_rules {
    bypass         = ["AzureServices", "Logging", "Metrics"]
    default_action = "Deny"
    ip_rules       = var.trusted_ips
  }

  tags = local.tags
}

resource "azurerm_role_assignment" "logging_network_owner" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_owner_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Storage Blob Data Owner"
}

resource "azurerm_role_assignment" "logging_network_reader" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_reader_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Storage Blob Data Reader"
}

resource "azurerm_role_assignment" "logging_keyvault_manager" {
  principal_id         = data.terraform_remote_state.identity.outputs.network_keyvault_manager_group_id
  scope                = azurerm_resource_group.hub_rg.id
  role_definition_name = "Storage Blob Data Reader"
}

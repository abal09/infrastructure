# from environment.auto.tfvars

variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

variable "subscription_id" {
  description = "Azure subscription id to assign resources to"
  type        = string
}

variable "primary_location" {
  description = "Azure primary location name"
  type        = string
}

variable "primary_location_code" {
  description = "devops short code for the primary location"
  type        = string
}

variable "trusted_ips" {
  description = "List of trusted IPs for remote access (e.g. key vault)"
  type        = list(string)
  default     = []
}

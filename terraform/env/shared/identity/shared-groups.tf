# Global Org-Based AD Groups
# --------------------------
#
#   ENG-Development         typical devs, broad access to dev but limited access to prod
#   ENG-Infrastructure      infrastructure team, broad access to all areas
#   ENG-ProductionSupport   for non-infra team members who require some prod access for support
#   ENG-Restricted          limited access for co-ops, or devs don't need deep infra access
#   CS-ProductionSupport    production support access for Customer Success
#

# Engineering

resource "azuread_group" "eng_development" {
  description  = "Typical developer access:  broad permissions for dev; limited access to prod"
  display_name = "ENG-Development"
}

resource "azuread_group" "eng_infrastructure" {
  description  = "Infrastructure team: broad permissions to all areas"
  display_name = "ENG-Infrastructure"
}

resource "azuread_group" "eng_prod_support" {
  description  = "Developer access needed for production support: expanded access to prod"
  display_name = "ENG-ProductionSupport"
}

resource "azuread_group" "eng_restricted" {
  description  = "Restricted developer access (interns, contractors): limited access to dev only"
  display_name = "ENG-Restricted"
}


# Customer Success

resource "azuread_group" "cs_prod_support" {
  description  = "Customer Success technical production support: limited access to prod"
  display_name = "CS-ProductionSupport"
}

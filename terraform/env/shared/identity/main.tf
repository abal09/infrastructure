terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "shared/identity.tfstate"
  }

  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 1.4.0"
    }
  }

  required_version = ">=1.0.0"
}

provider "azuread" {
}


# Existing Groups

data "azuread_group" "azure_admins" {
  display_name     = "Azure Admins"
  security_enabled = true
}

data "azuread_group" "engineering" {
  display_name     = "Engineering"
  security_enabled = true
}

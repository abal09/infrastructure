# Network Owners

resource "azuread_group_member" "azure_admins_network_owner" {
  group_object_id  = module.env_ad_groups.network_owner_group_id
  member_object_id = data.azuread_group.azure_admins.id
}

resource "azuread_group_member" "eng_infrastructure_network_owner" {
  group_object_id  = module.env_ad_groups.network_owner_group_id
  member_object_id = azuread_group.eng_infrastructure.id
}

# Network Readers

resource "azuread_group_member" "engineering_network_reader" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = data.azuread_group.engineering.id
}

resource "azuread_group_member" "eng_development_network_reader" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = azuread_group.eng_development.id
}

resource "azuread_group_member" "eng_prod_support_network_reader" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = azuread_group.eng_prod_support.id
}

resource "azuread_group_member" "eng_restricted_network_reader" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = azuread_group.eng_restricted.id
}

resource "azuread_group_member" "cs_prod_support_network_reader" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = azuread_group.cs_prod_support.id
}

# Network Keyvault Managers

resource "azuread_group_member" "azure_admins_network_keyvault_manager" {
  group_object_id  = module.env_ad_groups.network_keyvault_manager_group_id
  member_object_id = data.azuread_group.azure_admins.id
}

resource "azuread_group_member" "eng_infrastructure_network_keyvault_manager" {
  group_object_id  = module.env_ad_groups.network_keyvault_manager_group_id
  member_object_id = azuread_group.eng_infrastructure.id
}

# Network Keyvault Readers
#   (none)

# Global User Groups

output "cs_prod_support_group_id" {
  description = "CS-ProductionSupport user group id"
  value       = azuread_group.cs_prod_support.id
}

output "eng_development_group_id" {
  description = "ENG-Development user group id"
  value       = azuread_group.eng_development.id
}

output "eng_infrastructure_group_id" {
  description = "ENG-Infrastructure user group id"
  value       = azuread_group.eng_infrastructure.id
}

output "eng_prod_support_group_id" {
  description = "ENG-ProductionSupport user group id"
  value       = azuread_group.eng_prod_support.id
}

output "eng_restricted_group_id" {
  description = "ENG-Restricted user group id"
  value       = azuread_group.eng_restricted.id
}

# Access control for shared resources

output "network_keyvault_reader_group_id" {
  description = "Network key vault reader security group id"
  value       = module.env_ad_groups.network_keyvault_reader_group_id
}

output "network_keyvault_manager_group_id" {
  description = "Network key vault manager security group id"
  value       = module.env_ad_groups.network_keyvault_manager_group_id
}

output "network_reader_group_id" {
  description = "Network resource reader security group id"
  value       = module.env_ad_groups.network_reader_group_id
}

output "network_owner_group_id" {
  description = "Network resource owner security group id"
  value       = module.env_ad_groups.network_owner_group_id
}

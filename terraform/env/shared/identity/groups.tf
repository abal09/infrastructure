module "env_ad_groups" {
  source = "../../../modules/identity/environment-groups"

  environment                     = var.environment
  include_cluster_groups          = false
  include_network_keyvault_groups = true
}

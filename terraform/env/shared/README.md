# "shared" environment

Resources that are shared across all environments.  If starting from scratch, this environment's components should be applied first.

Recommended deployment order:
1. `identity` – User groups & assignments
2. `network` – Regional shared hub with VPN gateway

# from environment.auto.tfvars

variable "environment" {
  description = "Target environment (e.g. dev, prod) for this module"
  type        = string
}

variable "subscription_id" {
  description = "Azure subscription id to assign resources to"
  type        = string
}

variable "primary_location" {
  description = "Azure primary location name"
  type        = string
}

variable "primary_location_code" {
  description = "devops short code for the primary location"
  type        = string
}

variable "trusted_ips" {
  description = "List of trusted IPs for remote access (e.g. key vault)"
  type        = list(string)
  default     = []
}


# module-specific variables

variable "base_resource_name" {
  description = "Base resource name for the cluster and related resources"
  type        = string
}

variable "cluster_subnet_cidr" {
  description = "Cluster subnet address range (CIDR)"
  type        = string
}

variable "kubernetes_version" {
  description = "Kubernetes version to use"
  type        = string
  default     = "1.20.9"
}
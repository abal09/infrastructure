locals {
  cluster_name = "${var.environment}-${var.primary_location_code}-${var.base_resource_name}"
  pool_name    = "${local.cluster_name}-node"
  tags = {
    creator  = "terraform"
    env      = var.environment
    location = var.primary_location_code
    owner    = "devops@devops.io"
  }
}

module "cluster" {
  source = "../../../../modules/clusters/base"

  subscription_id          = var.subscription_id
  location                 = var.primary_location
  resource_group_name      = "${local.cluster_name}-rg"
  cluster_name             = "${local.cluster_name}-aks"
  node_resource_group_name = "${local.pool_name}-rg"
  vnet_id                  = data.terraform_remote_state.networks.outputs.spoke_vnet_id
  subnet_name              = "${local.cluster_name}-cluster-snet"
  subnet_cidr              = var.cluster_subnet_cidr
  kubernetes_version       = var.kubernetes_version

  reader_group_id  = data.terraform_remote_state.identity.outputs.cluster_reader_group_id
  manager_group_id = data.terraform_remote_state.identity.outputs.cluster_manager_group_id
  owner_group_id   = data.terraform_remote_state.identity.outputs.cluster_owner_group_id
  gitaccess_sp_id  = data.terraform_remote_state.identity.outputs.sp_object_id
  tags = local.tags
}

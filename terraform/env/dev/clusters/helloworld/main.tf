terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/clusters/helloworld.tfstate"
  }

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.76.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~>2.2.0"
    }
  }

  required_version = ">= 1.0.0"
}

# Azure - Main subscription
provider "azurerm" {
  subscription_id = var.subscription_id

  features {}
}

# Helm + Kubernetes + Kustomization - providers depend on cluster resource
data "azurerm_kubernetes_cluster" "aks" {
  name                = module.cluster.cluster_name
  resource_group_name = module.cluster.resource_group
}

provider "helm" {
  kubernetes {
    host                   = data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.host
    client_certificate     = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.client_certificate)
    client_key             = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.client_key)
    cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.cluster_ca_certificate)
  }
}

provider "kubernetes" {
  host                   = data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.host
  client_certificate     = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.client_certificate)
  client_key             = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.client_key)
  cluster_ca_certificate = base64decode(data.azurerm_kubernetes_cluster.aks.kube_admin_config.0.cluster_ca_certificate)
}

data "terraform_remote_state" "identity" {
  backend = "azurerm"
  config = {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/identity.tfstate"
  }
}

data "terraform_remote_state" "networks" {
  backend = "azurerm"
  config = {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/networks.tfstate"
  }
}
resource "kubernetes_namespace" "helloworld" {
  metadata {
    name = "${var.base_resource_name}"
  }
}

resource "helm_release" "helloworld-api" {
  name       = "${var.base_resource_name}"
  namespace  = "${var.base_resource_name}"
  repository = "${path.root}/../../../../../helm"
  chart      = "${var.base_resource_name}"
  version    = "1.0.0"

  values = [
    "${file("${path.root}/../../../../../helm/${var.base_resource_name}/values.yaml")}",
  ]

}

resource "helm_release" "nginx_ingress" {
  name      = "${var.base_resource_name}-nginx-ingress"
  namespace = "${var.base_resource_name}"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart     = "ingress-nginx"

  values = [
    "${file("${path.root}/../../../../../helm/nginx-ingress/nginx-ingress-values.yaml")}",
  ]
}

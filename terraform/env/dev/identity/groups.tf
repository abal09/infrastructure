module "env_ad_groups" {
  source = "../../../modules/identity/environment-groups"

  environment = var.environment
  include_network_keyvault_groups = true
}

module "tenant_ad_groups" {
  source = "../../../modules/identity/tenant-groups"

  environment = var.environment
}

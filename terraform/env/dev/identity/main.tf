terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/identity.tfstate"
  }

  required_providers {
    random = {
      source = "hashicorp/random"
      version = "~>3.1.0"
    }
  }

  required_version = ">= 1.0.0"
}

data "azuread_group" "azure_admins" {
  display_name     = "Azure Admins"
  security_enabled = true
}

data "azuread_group" "engineering" {
  display_name     = "Engineering"
  security_enabled = true
}

# "Azure Admins" memberships

resource "azuread_group_member" "azure_admins_cluster_owner" {
  group_object_id  = module.env_ad_groups.cluster_owner_group_id
  member_object_id = data.azuread_group.azure_admins.id
}

resource "azuread_group_member" "azure_admins_network_owner" {
  group_object_id  = module.env_ad_groups.network_owner_group_id
  member_object_id = data.azuread_group.azure_admins.id
}

resource "azuread_group_member" "azure_admins_network_keyvault_manager" {
  group_object_id  = module.env_ad_groups.network_keyvault_manager_group_id
  member_object_id = data.azuread_group.azure_admins.id
}

# "Engineering" memberships

resource "azuread_group_member" "engineering_cluster_manager" {
  group_object_id  = module.env_ad_groups.cluster_manager_group_id
  member_object_id = data.azuread_group.engineering.id
}

resource "azuread_group_member" "engineering_membership" {
  group_object_id  = module.env_ad_groups.network_reader_group_id
  member_object_id = data.azuread_group.engineering.id
}

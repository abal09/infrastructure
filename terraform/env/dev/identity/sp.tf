resource "random_id" "gitaccess" {
  byte_length = 8
  prefix      = "terraform-"
}

# Create Azure AD App
resource "azuread_application" "gitaccess" {
  display_name               = random_id.gitaccess.hex
  available_to_other_tenants = false
}

# Create Service Principal associated with the Azure AD App
resource "azuread_service_principal" "gitaccess" {
  application_id = azuread_application.gitaccess.application_id
}

# Generate random string to be used for Service Principal password
resource "random_string" "password" {
  length  = 32
  special = true
}

# Create Service Principal password
resource "azuread_service_principal_password" "gitaccess" {
  service_principal_id = azuread_service_principal.gitaccess.id
  value                = random_string.password.result
  end_date_relative    = "17520h"
}

output "cluster_reader_group_id" {
  description = "AKS cluster reader security group id"
  value       = module.env_ad_groups.cluster_reader_group_id
}

output "cluster_manager_group_id" {
  description = "AKS cluster manager security group id"
  value       = module.env_ad_groups.cluster_manager_group_id
}

output "cluster_owner_group_id" {
  description = "AKS cluster owner security group id"
  value       = module.env_ad_groups.cluster_owner_group_id
}

output "keyvault_reader_group_id" {
  description = "Tenant key vault reader security group id"
  value       = module.tenant_ad_groups.keyvault_reader_group_id
}

output "keyvault_manager_group_id" {
  description = "Tenant key vault manager security group id"
  value       = module.tenant_ad_groups.keyvault_manager_group_id
}

output "network_reader_group_id" {
  description = "Network resource reader security group id"
  value       = module.env_ad_groups.network_reader_group_id
}

output "network_owner_group_id" {
  description = "Network resource owner security group id"
  value       = module.env_ad_groups.network_owner_group_id
}

output "owner_group_id" {
  description = "Tenant resource owner security group id"
  value       = module.tenant_ad_groups.owner_group_id
}

output "storage_reader_group_id" {
  description = "Tenant storage reader security group id"
  value       = module.tenant_ad_groups.storage_reader_group_id
}

output "storage_manager_group_id" {
  description = "Tenant storage manager security group id"
  value       = module.tenant_ad_groups.storage_manager_group_id
}

output "sp_display_name" {
  value = azuread_service_principal.gitaccess.display_name
}

output "sp_client_id" {
  value = azuread_application.gitaccess.application_id
}

output "sp_client_secret" {
  value     = azuread_service_principal_password.gitaccess.value
  sensitive = true
}

output "sp_object_id" {
  value = azuread_service_principal.gitaccess.id
}
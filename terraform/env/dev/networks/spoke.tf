locals {
  spoke_name = "${var.environment}-${var.primary_location_code}-spoke01"

  tags = {
    creator  = "terraform"
    env      = var.environment
    location = var.primary_location_code
    owner    = "devops@devops.io"
  }
}

module "spoke_vnet" {
  source = "../../../modules/networks/spoke-vnet"

  environment         = var.environment
  subscription_id     = var.subscription_id
  location            = var.primary_location
  resource_group_name = "${local.spoke_name}-rg"
  vnet_name           = "${local.spoke_name}-vnet"
  vnet_cidr           = var.spoke_vnet_cidr
  hub_vnet_id         = data.terraform_remote_state.shared_networks.outputs.hub_vnet_id
  owner_group_id      = data.terraform_remote_state.identity.outputs.network_owner_group_id
  reader_group_id     = data.terraform_remote_state.identity.outputs.network_reader_group_id
  tags                = local.tags
}

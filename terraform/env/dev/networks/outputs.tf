output "spoke_vnet_id" {
  description = "Spoke virtual network id"
  value       = module.spoke_vnet.vnet_id
}

terraform {
  backend "azurerm" {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/networks.tfstate"
  }

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.76.0"
    }
  }

  required_version = ">= 1.0.0"
}

provider "azurerm" {
  subscription_id = var.subscription_id
  features {}
}

data "terraform_remote_state" "identity" {
  backend = "azurerm"
  config = {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "dev/identity.tfstate"
  }
}

data "terraform_remote_state" "shared_networks" {
  backend = "azurerm"
  config = {
    resource_group_name  = "terraform-admin-rg"
    storage_account_name = "devopsterraformadminst"
    container_name       = "terraform-state"
    key                  = "shared/networks.tfstate"
  }
}

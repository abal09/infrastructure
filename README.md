# Infrastructure-as-Code

This repo contains (almost) everything needed to spin up our infrastructure from scratch.
Application deployment is stored in `deployments`.

This repo is intended for use by the Cloud/DevOps team and assumes the user has the necessary permissions already.

## Repository Layout

At the top level, IaC files are stored by deployment technology:
```
  helm/
  terraform/
```

See the README file each subdirectory for specific layouts.

Ideally, Terraform is used as the primary deployment technology.  In cases where it needs to work with Helm charts, those charts should be stored int the root `helm/` directory, rather than being buried somewhere within `terraform/`.

`scripts` should be for anything that isn't yet deployed by Terraform (or for those resources required to bootstrap Terraform).  Trivial one-liners used by Terraform should be contained in the respective `.tf` file, but more complex scripts referred to by `.tf` files can be stored here too for easier reference.

## Setup / Installation

Install Terraform:
* `brew install terraform`

Setup to generate certificates:
* `brew install openssl`
* add the openssl version installed by homebrew to your path, e.g.:
  * `echo 'export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"' >> ~/.zshrc`
